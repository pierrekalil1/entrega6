import { Response, Request } from "express";
import { filterById, finishBuy } from "../services/buy.service";

export const BuyByIdController = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const buy = await filterById(id);
    res.status(200).json(buy);
  } catch (error: any) {
    res.status(400).json({ message: error.message });
  }
};

export const FinishBuy = async (req: Request, res: Response) => {
  const user = req.user;

  try {
    const buy = await finishBuy(user!.id);
    console.log(buy);

    return res.status(200).json(buy);
  } catch (error: any) {
    console.log(error);
    return res.status(400).json({ message: error.message });
  }
};
