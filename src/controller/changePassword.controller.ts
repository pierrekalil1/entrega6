import { Request, Response } from "express";
import { ChangePasswordService } from "../services/changePassword.service";

export const ChangePasswordController = async (req: Request, res: Response) => {
  const { password, code } = req.body;

  try {
    const updatedUser = await ChangePasswordService(code, password);

    res.status(200).json(updatedUser);
  } catch (error: any) {
    res.status(400).json({ message: error.message });
  }
};
