import { Request, Response, NextFunction } from "express";
import { allProducts, ById, createProduct } from "../services/product.service";

export const create = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const data = req.body;
    console.log(data);
    const product = await createProduct(data);
    console.log(product, "**************");
    res.send(product);
  } catch (err) {
    next(err);
  }
};

export const get = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const product = await ById(id);

    return res.send(product);
  } catch (err) {
    next(err);
  }
};

export const getAllProducts = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // const page = (req.query.page as any) ? parseInt(req.query.page as string) : 1;
  const products = await allProducts();

  console.log("oi", "++++++++++++++++++");
  res.status(201).send(products);
};
