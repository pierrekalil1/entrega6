import { NextFunction, Request, Response } from "express";
import { createUser, listUser, userById } from "../services/user.service";

export const create = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const data = req.validateData;
    const user = await createUser(data);
    res.send(user);
  } catch (err) {
    next(err);
  }
};

export const list = async (req: Request, res: Response, next: NextFunction) => {
  const page = (req.query.page as any) ? parseInt(req.query.page as string) : 1;
  const users = await listUser();
  console.log(users, "++++++++++++++++");
  res.send(users);
};

export const getUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    const user = await userById(id);

    res.send(user);
  } catch (err) {
    next(err);
  }
};
