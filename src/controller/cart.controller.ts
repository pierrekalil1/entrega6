import { Response, Request } from "express";
import {
  AddToCartService,
  GetCartService,
  ListCartService,
} from "../services/cart.service";
import UserRepository from "../repositories/userRepository";
import { getCustomRepository } from "typeorm";

export const AddToCartController = async (req: Request, res: Response) => {
  const { productId, userId }: any = req.body;

  try {
    const cartProducts = await AddToCartService(productId, userId);

    res.status(200).json(cartProducts);
  } catch (error: any) {
    res.status(400).json({ message: error.message });
  }
};

export const GetCartController = async (req: Request, res: Response) => {
  const userId = req.params.userId;

  // const user = req.user;
  const userRepository = getCustomRepository(UserRepository);
  const user: any = await userRepository.findOne(userId);

  if (userId !== user.id && user.isAdm !== true) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  try {
    const cartProducts = await GetCartService(userId);
    console.log(cartProducts, "*");

    return res.status(200).json(cartProducts);
  } catch (error: any) {
    res.status(400).json({ message: error.message });
  }
};

export const ListCartController = async (req: Request, res: Response) => {
  const userId = req.user;
  const userRepository = getCustomRepository(UserRepository);
  const user: any = await userRepository.findOne(userId);

  const cartsList = await ListCartService();

  return res.status(200).json(cartsList);
};
