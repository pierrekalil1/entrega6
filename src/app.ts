import "reflect-metadata";
import express from "express";

import { inittializerRouter } from "./routes";

const app = express();
app.use(express.json());

inittializerRouter(app);

app.get("/", (req, res) => {
  res.send({ message: "ok ok" });
});

export default app;
