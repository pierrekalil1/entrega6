import { EntityRepository, Repository } from "typeorm";
import { Buys } from "../entities/Buy";

@EntityRepository(Buys)
class BuysRepository extends Repository<Buys> {}

export { BuysRepository };
