import { EntityRepository, Repository } from "typeorm";
import { CartsProducts } from "../entities/Cart_Products";

@EntityRepository(CartsProducts)
class CartsProductsRepository extends Repository<CartsProducts> {}

export { CartsProductsRepository };
