import { EntityRepository, Repository } from "typeorm";
import { Products } from "../entities";

@EntityRepository(Products)
class ProductRepository extends Repository<Products> {
  public async getAll(): Promise<Products[] | undefined> {
    const products = await this.find();
    return products;
  }

  public async findById(id: string): Promise<Products | undefined> {
    const user = await this.findOne({
      where: {
        id,
      },
    });
    return user;
  }
}

export { ProductRepository };
