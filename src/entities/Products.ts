import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
} from "typeorm";
import { Buys } from "./Buy";
import { CartsProducts } from "./Cart_Products";

@Entity("products")
export default class Products {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  name!: string;

  @Column("float")
  price!: number;

  @OneToMany(() => CartsProducts, (cs) => cs.product)
  carts!: CartsProducts[];

  @ManyToOne(() => Buys, (buys) => buys.productsList)
  buys!: Buys;
}
