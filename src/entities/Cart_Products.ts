import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";
import { Cart, Products } from ".";

@Entity()
export class CartsProducts {
  @PrimaryColumn()
  cartId!: string;

  @PrimaryColumn()
  productId!: string;

  @Column()
  productQuantity: number;

  @ManyToOne(() => Cart, (cart) => cart.products)
  @JoinColumn({ name: "cartId" })
  cart!: Cart;

  @ManyToOne(() => Products, (product) => product.carts)
  @JoinColumn({ name: "productId" })
  product!: Products;

  constructor(quantity: number) {
    this.productQuantity = quantity;
  }
}
