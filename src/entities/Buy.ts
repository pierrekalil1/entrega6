import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
} from "typeorm";
import { Products } from ".";

@Entity()
export class Buys {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @OneToMany((type) => Products, (product) => product.buys, {
    cascade: true,
  })
  productsList: Products[];

  @Column("float")
  total: number;

  @CreateDateColumn()
  date!: Date;

  constructor(productsList: Products[], total: number) {
    this.productsList = productsList;
    this.total = total;
  }
}
