import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  ManyToMany,
  JoinTable,
  BeforeInsert,
  OneToMany,
  JoinColumn,
} from "typeorm";
import { CartsProducts } from "./Cart_Products";
import Products from "./Products";
import User from "./User";

@Entity("cart")
export default class Cart {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @OneToMany(() => CartsProducts, (cs) => cs.cart)
  products!: CartsProducts[];

  @OneToOne(() => User, (user) => user.cart)
  @JoinColumn()
  user!: User;

  @BeforeInsert()
  startProducts() {
    this.products = [];
  }
}
