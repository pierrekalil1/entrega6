import * as yup from "yup";

export const ProductSchema = yup.object().shape({
  name: yup.string().required(),
  price: yup.number().required(),
});
