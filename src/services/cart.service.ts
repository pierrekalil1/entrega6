import { getCustomRepository } from "typeorm";
import { CartsProductsRepository } from "../repositories/cartProductRepository";
import { CartsRepository } from "../repositories/cartRepository";
import { ProductRepository } from "../repositories/productRepository";

export const AddToCartService = async (productId: string, userId: string) => {
  const cartsProductsRepository = getCustomRepository(CartsProductsRepository);
  const cartsRepository = getCustomRepository(CartsRepository);
  const productsRepository = getCustomRepository(ProductRepository);

  const cart = await cartsRepository.findOne({ where: { user: userId } });
  const product = await productsRepository.findOne(productId);

  const cpRepository = await cartsProductsRepository.findOne({
    cartId: cart?.id,
    productId: productId,
  });

  if (!cpRepository) {
    const connection = cartsProductsRepository.create({
      cart: cart,
      product: product,
      productQuantity: 1,
    });
    await cartsProductsRepository.save(connection);
  } else {
    await cartsProductsRepository.update(cpRepository, {
      productQuantity: cpRepository.productQuantity + 1,
    });
  }

  const cartProducts = await cartsProductsRepository.find({
    cartId: cart?.id,
  });

  return cartProducts;
};

export const GetCartService = async (userId: string) => {
  console.log(userId);
  const cartsRepository = getCustomRepository(CartsRepository);
  const cartsProductsRepository = getCustomRepository(CartsProductsRepository);

  const cart = await cartsRepository.findOne({ where: { user: userId } });

  if (!cart) {
    throw new Error("This cart does not exist");
  }

  const cartProducts = await cartsProductsRepository.find({
    cartId: cart.id,
  });

  return cartProducts;
};

export const ListCartService = async () => {
  const cartsRepository = getCustomRepository(CartsRepository);

  const cartsList = await cartsRepository.find();

  return cartsList;
};
