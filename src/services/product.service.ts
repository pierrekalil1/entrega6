import { getCustomRepository, getRepository } from "typeorm";
import { Products } from "../entities";
import AppError from "../errors/App.Error";
import { ProductRepository } from "../repositories/productRepository";

interface ProductsBody {
  name: string;
  price: number;
}

export const createProduct = async (body: ProductsBody) => {
  const { name, price } = body;

  try {
    const productRepository = getCustomRepository(ProductRepository);
    const productExists = await productRepository.findOne({ name });

    if (productExists) {
      throw new AppError("Product already exists", 409);
    }
    const product = productRepository.create({
      name,
      price,
    });
    await productRepository.save(product);
    return product;
  } catch (err) {
    return err;
  }
};

export const allProducts = async () => {
  const productRepository = getCustomRepository(ProductRepository);
  const products = productRepository.getAll();
  return products;
};

export const ById = async (id: string) => {
  try {
    const productRepository = getRepository(Products);
    const product = await productRepository.findOne(id);
    if (!product) {
      throw new AppError("product not found", 404);
    }
    return product;
  } catch (err) {
    return { message: err };
  }
};
