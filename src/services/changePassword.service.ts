import UserRepository from "../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcrypt";

export const ChangePasswordService = async (
  code: string,
  newPassword: string
) => {
  const usersRepository = getCustomRepository(UserRepository);

  const user = await usersRepository.findOne({ where: { resetCode: code } });
  if (!user) {
    throw new Error("This user does not exist");
  }

  user.password = await bcrypt.hashSync(newPassword, 10);

  await usersRepository.update(user.id, user);

  return user;
};
