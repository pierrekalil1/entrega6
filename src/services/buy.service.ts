import { getCustomRepository } from "typeorm";
import { BuysRepository } from "../repositories/buyRepository";
import { CartsRepository } from "../repositories/cartRepository";
import { CartsProductsRepository } from "../repositories/cartProductRepository";
import { ProductRepository } from "../repositories/productRepository";
import AppError from "../errors/App.Error";

export const filterById = async (id: string) => {
  const buysRepository = getCustomRepository(BuysRepository);

  const buy = await buysRepository.findOne(id);

  if (!buy) {
    throw new Error("This buy does not exist");
  }

  return buy;
};

export const finishBuy = async (userId: string) => {
  const buysRepository = getCustomRepository(BuysRepository);
  const cartsProductsRepository = getCustomRepository(CartsProductsRepository);
  const productsRepository = getCustomRepository(ProductRepository);
  const cartsRepository = getCustomRepository(CartsRepository);

  const cart = await cartsRepository.findOne({ where: { user: userId } });

  if (!cart) {
    throw new AppError("This cart does not exist", 400);
  }
  const productsList = await cartsProductsRepository.find({
    cartId: cart.id,
  });
  console.log(productsList);
  let total = 0;
  let list = [];

  for (const cp of productsList) {
    const item = await productsRepository.findOne(cp.productId);

    if (item) {
      list.push(item);
      total += item.price * cp.productQuantity;
    }
  }

  const newBuy = {
    productsList: list,
    total: total,
  };

  const buy = buysRepository.create(newBuy);
  await buysRepository.save(buy);
  return buy;
};
