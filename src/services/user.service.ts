import { getRepository, getCustomRepository } from "typeorm";
import { User } from "../entities";
import AppError from "../errors/App.Error";
import { CartsRepository } from "../repositories/cartRepository";
import UserRepository from "../repositories/userRepository";

interface UserBody {
  name?: string;
  email?: string;
  password?: string;
  isAdm?: boolean;
}

export const createUser = async (body: UserBody) => {
  const { name, email, password, isAdm } = body;

  try {
    const userRepository = getRepository(User);
    const cartsRepository = getCustomRepository(CartsRepository);

    const user = userRepository.create({
      name,
      email,
      password,
      isAdm,
    });

    await userRepository.save(user);

    const cart = cartsRepository.create({ user });
    console.log(cart);
    await cartsRepository.save(cart);

    return user;
  } catch (err) {
    throw new AppError(`E-mail already registered`, 404);
  }
};

export const listUser = async (page = 1) => {
  const userRepository = getCustomRepository(UserRepository);
  const users = await userRepository.paginated(page);
  console.log(users);
  return users;
};

export const userById = async (id: string) => {
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.getById(id);
  if (!user) {
    throw new AppError("User not found", 404);
  }
  return user;
};
