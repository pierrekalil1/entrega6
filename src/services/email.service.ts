import nodemailer from "nodemailer";

export const transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "e708cbd56057d2",
    pass: "d3d7372f6ccbba",
  },
});

export const mailOptions = (to: string, subject: string, text: string) => {
  return {
    from: "no-repply@entrega6.com",
    to,
    subject,
    text,
  };
};
