import app from "./app";
import { connectDatabase } from "./database";

connectDatabase();

app.listen(4000, () => {
  console.log("Running at http://localhost:4000");
});
