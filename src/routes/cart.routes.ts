import { Router } from "express";

import {
  AddToCartController,
  GetCartController,
  ListCartController,
} from "../controller/cart.controller";
import { isAuthenticated } from "../middleware/authenticated.middleware";
import { isAdmin } from "../middleware/admin.middleware";

const router = Router();

export const cartRouter = () => {
  router.post("", AddToCartController);
  router.get("/:userId", GetCartController);
  router.get("", isAdmin, ListCartController);

  return router;
};
