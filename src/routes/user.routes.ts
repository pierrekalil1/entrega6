import { Router } from "express";
import { create, list } from "../controller/user.controller";
import { isAdmin } from "../middleware/admin.middleware";
import { isAuthenticated } from "../middleware/authenticated.middleware";
import { validate } from "../middleware/validate.middleware";
import { UserSchema } from "../schemas/UserSchema";
import { userById } from "../services/user.service";

const router = Router();

export const userRouter = () => {
  router.post("", validate(UserSchema), create);
  router.get("/:id", userById);
  router.get("", isAuthenticated, isAdmin, list);
  return router;
};
