import { Router } from "express";
import { ChangePasswordController } from "../controller/changePassword.controller";
import { isAuthenticated } from "../middleware/authenticated.middleware";
const router = Router();

export const changePasswordRouter = () => {
  router.post("", isAuthenticated, ChangePasswordController);
  return router;
};
