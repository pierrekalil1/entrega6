import { Express } from "express";
import { loginRouter } from "./login.routes";
import { userRouter } from "./user.routes";
import { productRouter } from "./product.routes";
import { cartRouter } from "./cart.routes";
import { buyRouter } from "./buy.routes";
import { changePasswordRouter } from "./changePassword.routes";

export const inittializerRouter = (app: Express) => {
  app.use("/login", loginRouter());
  app.use("/users", userRouter());
  app.use("/product", productRouter());
  app.use("/cart", cartRouter());
  app.use("/buy", buyRouter());
  app.use("/password", changePasswordRouter())
};
