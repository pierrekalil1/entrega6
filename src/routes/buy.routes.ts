import { Router } from "express";
import { BuyByIdController, FinishBuy } from "../controller/buy.controller";
import { isAuthenticated } from "../middleware/authenticated.middleware";

const router = Router();

export const buyRouter = () => {
  router.post("", isAuthenticated, FinishBuy);
  router.get("/:id", BuyByIdController);
  router.get("");
  return router;
};
