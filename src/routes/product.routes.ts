import { Router } from "express";
import { create, get, getAllProducts } from "../controller/product.controller";
import { isAdmin } from "../middleware/admin.middleware";
import { isAuthenticated } from "../middleware/authenticated.middleware";
import { validate } from "../middleware/validate.middleware";
import { ProductSchema } from "../schemas/ProductSchema";

const router = Router();

export const productRouter = () => {
  // router.post("", isAuthenticated, isAdmin, validate(ProductSchema), create);
  router.post("", create);
  router.get("/:id", get);
  router.get("", getAllProducts);
  return router;
};
