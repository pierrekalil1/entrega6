import { createConnection, getConnection } from "typeorm";
import request from "supertest";
import app from "../app";

describe("User Test", () => {
  beforeAll(async () => {
    await createConnection();
  });

  afterAll(async () => {
    const connection = getConnection();

    await connection.close();
  });

  const testUser = {
    name: "Pierre",
    email: "pierre@mail.com",
    password: "123456@Az",
    isAdm: false,
  };

  let token = "";
  let userId = "";

  it("Should create the new user in the database", async () => {
    const response = await request(app).post("/user").send(testUser);

    userId = response.body.id;

    expect(response.status);
  });
});
