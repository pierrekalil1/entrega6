# Entrega 6

A [Entrega 6] sistema de cadastro de usuarios com validações de senha e formulário.

## Instalação/Utilização

Para ter acesso à estrutura da API, faça o fork e depois clone este projeto.

## Rotas

<h3 align='center'> Cadastro de usuário</h3>

`POST /user - para cadastro de usuários FORMATO DA REQUISIÇÃO `

```json
{
	"name": "Pierre",
	"email": "pierre@mail.com",
	"password": "123456@Az",
	"isAdm": true

```

Caso dê tudo certo, a resposta será assim:

`POST /user - FORMATO DA RESPOSTA - STATUS 201`

```json
{
  "uuid": "69fd87d7-259c-4d22-9e63-42f9da8ffcf0",
  "name": "Pierre2",
  "email": "pierre2@mail.com",
  "isAdmin": true
}
```

<h3 align='center'> Login de usuário</h3>
`POST /login - para login de usuários FORMATO DA REQUISIÇÃO `

```json
{
  "email": "pierre@mail.com",
  "password": "123456@Az"
}
```

Caso dê tudo certo, a resposta será assim:

`POST /login - FORMATO DA RESPOSTA - STATUS 200`

```json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjlhYmE3ZDJhLThlYWItNDZjMC05NWFjLTQ2MmQ5ZDU1MWM0OSIsImlhdCI6MTY0NDU0MDc1NywiZXhwIjoxNjQ0NjI3MTU3fQ.4mzcLIBQ4pTR0CAVrXLtr49yGp9GzfNWmp828Pr-TaU"
}
```

<h3 align='center'> Cadastro de produto</h3>

`POST /product - para cadastro de usuários FORMATO DA REQUISIÇÃO `

```json
{
  "name": "Som",
  "category": "Eltro",
  "price": 10.0
}
```

Caso dê tudo certo, a resposta será assim:

`POST /product - FORMATO DA RESPOSTA - STATUS 201`

```json
{
  "name": "Som",
  "category": "Eltro",
  "price": 10,
  "id": 5
}
```

<h3 align='center'> Pesquisa de produto</h3>

`GET /product - para cadastro de usuários FORMATO DA REQUISIÇÃO `

```json
No body

```

Caso dê tudo certo, a resposta será assim:

`GET /product - FORMATO DA RESPOSTA - STATUS 201`

```json
{
  "id": 1,
  "name": "Pilha",
  "category": "Eltro",
  "price": 10
}
```
